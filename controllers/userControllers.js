const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if th email exists
/*

	Steps:
	1. Use mongoose "find" method to find the duplicate emails
	2. Use the .then method to send back response to front end

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		//email is found
		if(result.length>0){
			return true;
		}
		//no duplicate email found
		else{
			return false;
		}
	});
}

// User registration
/*

	Steps:
	1. Create a new User object using the mongoose model and the information from the request body.
	2. Make sure that the password is encrypted.
	3. Save the new User to the database

*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10),	
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user,error) => {
		if(error){
			return false;
		}
		else {
			console.log(user);
			return true;
		}
	})
}

// User Login
/*

	1. check the database if the user exists
	2. Compare the password provided in the login form with the password stored in the database
	3. create token

*/
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		else {
			// compare reqBody.passowrd and result.password
			// bcrypt.compareSync() returns boolean results
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}

// ACTIVITY S32-S33
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		else {
			result.password = "";
			return result;
		}
	})
}

/*

	Alternate solution:
	module.exports.getProfile = (data) => {
		return User.findById(data.userId).then(result => {
			result.password = "";
			return result;
		})
	}

*/



//Enroll user to a class
/*

	Steps:
	1. Find the document in the database using user's ID
	2. Add the courseId to the user's enrollments array
	3. Add the userId to the course's enrolees array
	4. Update the document in the MongoDB Atlas database


*/

// Async await will be used in enrolling the user because we will need to update 2 separate documents (users,courses) when enrolling a user.

module.exports.enroll = async (data) => {
																//result
	let isUserUpdated = await User.findById(data.userId).then( user => {
		// Add the courseId in the user's enrollment array
		user.enrollments.push({courseId: data.courseId});

		// Saves the updated user information in the database.
		return user.save().then((enrollment,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{
		course.enrollees.push({userId: data.userId});
		course.slots = -1;

		return course.save().then((enrollees,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	console.log(isCourseUpdated);

	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	else{
		return false;
	}
}
