const Course = require("../models/Course");

// Create a new course
/*

	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.


*/

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	});

	return newCourse.save().then((course,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Retrieve all courses
/*

	Step:
	1. Retrieve all the courses from the database


*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result);
}

// Retrieve all active courses
/*

	Retrieve all courses from database with property of isActive to true

*/

module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => result);
}

//Retrieve a specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => result);
}


// Update information for courses

/*

	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the courseId retrieved from request params/url property and the variable "updatedCourse" containing the information from the request body

*/

module.exports.updateCourse = (courseId, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	// findByIdAndUpdate(documentId, updatesToBeApplied)

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//Archive a course

module.exports.archiveCourse = (courseId,reqBody) => {
	let updateActiveField = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(courseId,updateActiveField).then((courseArchive,errorArchive) => {
		if(errorArchive){
			return false;
		}
		else{
			return true;
		}
	})
}