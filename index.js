// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Create Server
const app = express();
const port = 4000;

//mongodb
mongoose.connect("mongodb+srv://admin:admin@coursebooking.m6muw.mongodb.net/course-booking-app?retryWrites=true&w=majority", {
		useNewUrlParser: true, 
		useUnifiedTopology: true
	});

// set notification for connection
let db = mongoose.connection;

// Notify on error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//Routes for our API
// localhost:4000/users
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// Listen to port
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})


