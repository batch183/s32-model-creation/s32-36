const jwt = require("jsonwebtoken");

// JSON Web Tokens
/*

	Analogy
	- Pack the gift provided with 

*/

const secret = "crushAkoNgCrushKo";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}

/*

	Syntax: jwt.sign(payload, secretOrPrivateKey, [options/callBackFunctions])

*/

// Token Verification
/*

	Analogy
		Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with.

*/

// Middleware Functions
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	console.log(typeof token);

	if(typeof token !== "undefined"){
		console.log(token);
		// This removes the "Bearer " prefix and obtains only the token for verification
		token = token.slice(7, token.length);
		console.log(token);

		// Syntax: jwt.verify(token, secret, [options/callBackFunction])
		return jwt.verify(token,secret,(err,data)=>{
			//If JWT is not valid
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{
				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}
}

// Token decryption
/*

	Analogy
		Open the gift and get the content.

*/
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return null;
			}
			else{
				// Syntax:
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}