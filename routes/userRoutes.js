const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");


// Router for checking if the email exists
router.post("/checkEmail", (req,res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//Router for user registration
router.post("/register", (req,res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for the user login
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//ACTIVITY S32 - S33

/*router.post("/details", (req,res) => {
	userControllers.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})*/

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData); // contains the token
	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile(userData).then(resultFromController => res.send(resultFromController));
})


// Route to enroll a course
router.post("/enroll", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {
		// userId will be retrieved from the request header
		userId: userData.id,
		courseId: req.body.courseId
	}
	if(userData.isAdmin){
		res.send("You're not allowed to access this page!")
	}
	else{
		userControllers.enroll(data).then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;
